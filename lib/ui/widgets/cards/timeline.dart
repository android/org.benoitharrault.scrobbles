import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_timeline_cubit.dart';
import 'package:scrobbles/models/data/timeline.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/timeline_counts.dart';
import 'package:scrobbles/ui/widgets/charts/timeline_eclecticism.dart';

class CardTimeline extends StatelessWidget {
  const CardTimeline({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount = settings.getAsInt(ApplicationConfig.parameterCodeTimelineDaysCount);

    return BlocBuilder<DataTimelineCubit, DataTimelineState>(
      builder: (BuildContext context, DataTimelineState data) {
        final TimelineData timeline = data.timeline ?? TimelineData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'timeline_title'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: Stack(
            children: [
              ChartTimelineCounts(chartData: timeline),
              ChartTimelineEclecticism(chartData: timeline),
            ],
          ),
        );
      },
    );
  }
}
