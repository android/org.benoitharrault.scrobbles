import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_new_tracks_cubit.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';

class CardNewTracks extends StatelessWidget {
  const CardNewTracks({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataNewTracksCubit, DataNewTracksState>(
      builder: (BuildContext context, DataNewTracksState data) {
        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'new_tracks_title'.tr(),
          status: data.status,
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: data.newTracks?.data
                    .map((newTrack) => Text(
                        '${newTrack.track?.artist.name ?? ''} - ${newTrack.track?.name ?? ''}'))
                    .toList() ??
                [],
          ),
        );
      },
    );
  }
}
