import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_top_artists_cubit.dart';
import 'package:scrobbles/models/data/topartists.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/top_artists.dart';
import 'package:scrobbles/ui/widgets/charts/top_artists_stream.dart';

class CardTopArtists extends StatelessWidget {
  const CardTopArtists({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeTopArtistsDaysCount);

    return BlocBuilder<DataTopArtistsCubit, DataTopArtistsState>(
      builder: (BuildContext context, DataTopArtistsState data) {
        final TopArtistsData artistsData = data.topArtists ?? TopArtistsData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'top_artists_title'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ChartTopArtists(chartData: artistsData),
              const SizedBox(height: 8),
              ChartTopArtistsStream(chartData: artistsData),
            ],
          ),
        );
      },
    );
  }
}
