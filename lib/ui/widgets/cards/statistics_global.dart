import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_statistics_global_cubit.dart';
import 'package:scrobbles/models/data/statistics_global.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/content/statistics_global.dart';

class CardStatisticsGlobal extends StatelessWidget {
  const CardStatisticsGlobal({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataStatisticsGlobalCubit, DataStatisticsGlobalState>(
      builder: (BuildContext context, DataStatisticsGlobalState data) {
        final StatisticsGlobalData statistics =
            data.statisticsGlobal ?? StatisticsGlobalData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.primary,
          title: 'global_statistics'.tr(),
          status: data.status,
          content: ContentStatisticsGlobal(statistics: statistics),
        );
      },
    );
  }
}
