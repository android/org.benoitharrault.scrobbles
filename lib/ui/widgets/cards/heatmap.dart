import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_heatmap_cubit.dart';
import 'package:scrobbles/models/data/heatmap.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/heatmap.dart';

class CardHeatmap extends StatelessWidget {
  const CardHeatmap({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    return BlocBuilder<DataHeatmapCubit, DataHeatmapState>(
      builder: (BuildContext context, DataHeatmapState data) {
        final HeatmapData heatmap = data.heatmap ?? HeatmapData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'heatmap'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: ChartHeatmap(chartData: heatmap),
        );
      },
    );
  }
}
