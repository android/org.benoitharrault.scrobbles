import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_counts_by_day_cubit.dart';
import 'package:scrobbles/models/data/counts_by_day.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/counts_by_day.dart';

class CardCountsByDay extends StatelessWidget {
  const CardCountsByDay({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    return BlocBuilder<DataCountsByDayCubit, DataCountsByDayState>(
      builder: (BuildContext context, DataCountsByDayState data) {
        final CountsByDayData counts = data.countsByDay ?? CountsByDayData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'counts_by_day'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: ChartCountsByDay(chartData: counts),
        );
      },
    );
  }
}
