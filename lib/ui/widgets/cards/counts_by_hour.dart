import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_counts_by_hour_cubit.dart';
import 'package:scrobbles/models/data/counts_by_hour.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/counts_by_hour.dart';

class CardCountsByHour extends StatelessWidget {
  const CardCountsByHour({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    return BlocBuilder<DataCountsByHourCubit, DataCountsByHourState>(
      builder: (BuildContext context, DataCountsByHourState data) {
        final CountsByHourData counts = data.countsByHour ?? CountsByHourData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'counts_by_hour'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: ChartCountsByHour(chartData: counts),
        );
      },
    );
  }
}
