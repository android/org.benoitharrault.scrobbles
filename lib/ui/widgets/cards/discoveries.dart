import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_discoveries_cubit.dart';
import 'package:scrobbles/models/data/discoveries.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/charts/discoveries_artists.dart';
import 'package:scrobbles/ui/widgets/charts/discoveries_tracks.dart';

class CardDiscoveries extends StatelessWidget {
  const CardDiscoveries({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDiscoveriesDaysCount);

    return BlocBuilder<DataDiscoveriesCubit, DataDiscoveriesState>(
      builder: (BuildContext context, DataDiscoveriesState data) {
        final TextTheme textTheme = Theme.of(context).primaryTextTheme;

        final DiscoveriesData discoveries = data.discoveries ?? DiscoveriesData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'discoveries_title'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'discoveries_artists_title',
                style: textTheme.titleMedium!.apply(fontWeightDelta: 2),
              ).tr(),
              const SizedBox(height: 8),
              ChartDiscoveriesArtists(chartData: discoveries),
              const SizedBox(height: 8),
              Text(
                'discoveries_tracks_title',
                style: textTheme.titleMedium!.apply(fontWeightDelta: 2),
              ).tr(),
              const SizedBox(height: 8),
              ChartDiscoveriesTracks(chartData: discoveries),
            ],
          ),
        );
      },
    );
  }
}
