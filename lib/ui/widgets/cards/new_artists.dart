import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_new_artists_cubit.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';

class CardNewArtists extends StatelessWidget {
  const CardNewArtists({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DataNewArtistsCubit, DataNewArtistsState>(
      builder: (BuildContext context, DataNewArtistsState data) {
        return CardContent(
          color: Theme.of(context).colorScheme.surface,
          title: 'new_artists_title'.tr(),
          status: data.status,
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: data.newArtists?.data
                    .map((newArtist) => Text(newArtist.artist?.name ?? ''))
                    .toList() ??
                [],
          ),
        );
      },
    );
  }
}
