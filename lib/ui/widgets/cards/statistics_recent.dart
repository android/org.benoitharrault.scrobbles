import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/data_statistics_recent_cubit.dart';
import 'package:scrobbles/models/data/statistics_recent.dart';
import 'package:scrobbles/ui/widgets/card_content.dart';
import 'package:scrobbles/ui/widgets/content/statistics_recent.dart';

class CardStatisticsRecent extends StatelessWidget {
  const CardStatisticsRecent({super.key});

  @override
  Widget build(BuildContext context) {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeStatisticsRecentDaysCount);

    return BlocBuilder<DataStatisticsRecentCubit, DataStatisticsRecentState>(
      builder: (BuildContext context, DataStatisticsRecentState data) {
        final StatisticsRecentData statistics =
            data.statisticsRecent ?? StatisticsRecentData.fromJson({});

        return CardContent(
          color: Theme.of(context).colorScheme.primary,
          title: 'recent_statistics'.tr(
            namedArgs: {
              'daysCount': daysCount.toString(),
            },
          ),
          status: data.status,
          content: ContentStatisticsRecent(statistics: statistics),
        );
      },
    );
  }
}
