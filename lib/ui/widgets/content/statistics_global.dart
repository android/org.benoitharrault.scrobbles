import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/models/data/statistics_global.dart';

class ContentStatisticsGlobal extends StatelessWidget {
  const ContentStatisticsGlobal({super.key, required this.statistics});

  final StatisticsGlobalData statistics;

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).primaryTextTheme;
    const String placeholder = '⏳';

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'statistics_total_scrobbles_count',
          style: textTheme.bodyMedium,
        ).tr(
          namedArgs: {
            'count':
                statistics.totalCount != null ? statistics.totalCount.toString() : placeholder,
          },
        ),
        Text(
          'statistics_last_scrobble',
          style: textTheme.bodyMedium,
        ).tr(
          namedArgs: {
            'datetime': statistics.lastScrobble != null
                ? DateFormat().format(statistics.lastScrobble ?? DateTime.now())
                : placeholder,
          },
        ),
      ],
    );
  }
}
