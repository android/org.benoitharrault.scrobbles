import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/models/data/statistics_recent.dart';

class ContentStatisticsRecent extends StatelessWidget {
  const ContentStatisticsRecent({super.key, required this.statistics});

  final StatisticsRecentData statistics;

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).primaryTextTheme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          (statistics.recentCount != null &&
                  statistics.firstPlayedArtistsCount != null &&
                  statistics.firstPlayedTracksCount != null)
              ? 'statistics_recent_scrobbles_count_and_discoveries'
              : '',
          style: textTheme.bodyMedium,
        ).tr(
          namedArgs: {
            'count': statistics.recentCount.toString(),
            'artistsCount': statistics.firstPlayedArtistsCount.toString(),
            'tracksCount': statistics.firstPlayedTracksCount.toString(),
          },
        ),
      ],
    );
  }
}
