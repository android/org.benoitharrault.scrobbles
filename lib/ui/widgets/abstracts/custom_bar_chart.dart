import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/ui/widgets/abstracts/custom_chart.dart';

class CustomBarChart extends CustomChart {
  const CustomBarChart({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: chartHeight,
    );
  }

  BarChart getBarChart({barWidth, backgroundColor}) {
    return BarChart(
      BarChartData(
        barGroups: getDataCounts(barWidth),
        backgroundColor: backgroundColor,
        borderData: simpleBorderData,
        gridData: horizontalGridData,
        titlesData: getTitlesData(),
        barTouchData: BarTouchData(enabled: false),
        maxY: getNextRoundNumber(getMaxCountsValue(), verticalTicksInterval),
      ),
    );
  }

  double getMaxCountsValue() {
    return 0.0;
  }

  double getBarWidth(double containerWidth, int barsCount) {
    return 0.65 * (containerWidth / barsCount);
  }

  List<BarChartGroupData> getDataCounts(double barWidth) {
    return [];
  }

  LinearGradient getGradient(Color baseColor, double value, double maxValue) {
    double alignmentTopValue = value != 0.0 ? -2 * maxValue / value + 1 : 0;

    return LinearGradient(
      begin: Alignment(-1, alignmentTopValue),
      end: const Alignment(1, 1),
      colors: <Color>[
        baseColor.lighten(30),
        baseColor,
        baseColor.darken(30),
      ],
      tileMode: TileMode.mirror,
    );
  }

  BarChartGroupData getBarItem(
      {required int x,
      required List<double> values,
      required List<Color> barColors,
      required double barWidth}) {
    List<BarChartRodData> barRods = [];

    for (int i = 0; i < values.length; i++) {
      double value = values[i];
      Color barColor = barColors[i];

      final gradient = getGradient(barColor, value, getMaxCountsValue());
      final borderColor = barColor.darken(20);

      barRods.add(
        BarChartRodData(
          toY: value,
          color: barColor,
          gradient: gradient,
          width: barWidth,
          borderRadius: const BorderRadius.all(Radius.zero),
          borderSide: BorderSide(
            color: borderColor,
          ),
        ),
      );
    }

    return BarChartGroupData(
      x: x,
      barRods: barRods,
    );
  }

  @override
  Widget getHorizontalTitlesWidget(double value, TitleMeta meta) {
    return getHorizontalTitlesWidgetWithDate(value, meta);
  }

  @override
  Widget getVerticalLeftTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesWidgetWithValue(value, meta);
  }

  @override
  Widget getVerticalRightTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesWidgetWithValue(value, meta);
  }
}
