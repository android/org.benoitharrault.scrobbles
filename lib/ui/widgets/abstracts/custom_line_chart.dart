import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'package:scrobbles/ui/widgets/abstracts/custom_chart.dart';

class CustomLineChart extends CustomChart {
  const CustomLineChart({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: chartHeight,
    );
  }

  Map<String, double> getHorizontalScaleFromDates(Iterable datesAsString) {
    double minDateAsDouble = double.maxFinite;
    double maxDateAsDouble = -double.maxFinite;

    for (var element in datesAsString) {
      final double date = DateTime.parse(element).millisecondsSinceEpoch.toDouble();

      if (date < minDateAsDouble) {
        minDateAsDouble = date;
      }
      if (date > maxDateAsDouble) {
        maxDateAsDouble = date;
      }
    }

    return {
      'min': minDateAsDouble,
      'max': maxDateAsDouble,
    };
  }

  @override
  Widget getVerticalLeftTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesWidgetWithValue(value, meta);
  }

  @override
  Widget getVerticalRightTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesWidgetWithValue(value, meta);
  }
}
