import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/app_colors.dart';

class CustomChart extends StatelessWidget {
  const CustomChart({super.key});

  static const double defaultChartHeight = 150.0;
  static const double defaultVerticalTicksInterval = 10;
  static const String defaultVerticalAxisTitleSuffix = '';
  static const double defaultTitleFontSize = 9;

  double get chartHeight => defaultChartHeight;
  double get verticalTicksInterval => defaultVerticalTicksInterval;
  String get verticalAxisTitleSuffix => defaultVerticalAxisTitleSuffix;
  double get titleFontSize => defaultTitleFontSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: chartHeight,
    );
  }

  double getNextRoundNumber(double number, double scale) {
    return scale * ((number ~/ scale).toInt() + 1);
  }

  Color getColorFromIndex(int index) {
    const List<int> hexValues = [
      0x8dd3c7,
      0xffffb3,
      0xbebada,
      0xfb8072,
      0x80b1d3,
      0xfdb462,
      0xb3de69,
      0xfccde5,
      0xd9d9d9,
      0xbc80bd,
      0xccebc5,
      0xffed6f,
    ];

    return Color(hexValues[index % hexValues.length] + 0xff000000);
  }

  String getDayShortName(int dayIndex) {
    switch (dayIndex) {
      case 1:
        return 'MON';
      case 2:
        return 'TUE';
      case 3:
        return 'WED';
      case 4:
        return 'THU';
      case 5:
        return 'FRI';
      case 6:
        return 'SAT';
      case 7:
        return 'SUN';
      default:
    }

    return '';
  }

  FlBorderData get noBorderData {
    return FlBorderData(
      show: false,
    );
  }

  FlBorderData get simpleBorderData {
    return FlBorderData(
      show: true,
      border: Border.all(
        color: AppColors.borderColor,
        width: 2,
      ),
    );
  }

  FlGridData get noGridData {
    return const FlGridData(
      show: false,
    );
  }

  FlGridData get horizontalGridData {
    return const FlGridData(
      show: true,
      drawHorizontalLine: true,
      drawVerticalLine: false,
    );
  }

  FlTitlesData getTitlesData() {
    const AxisTitles none = AxisTitles(
      sideTitles: SideTitles(showTitles: false),
    );

    final AxisTitles verticalTitlesLeft = AxisTitles(
      sideTitles: SideTitles(
        showTitles: true,
        reservedSize: 35,
        getTitlesWidget: getVerticalLeftTitlesWidget,
        interval: verticalTicksInterval,
      ),
    );

    final AxisTitles verticalTitleRight = AxisTitles(
      sideTitles: SideTitles(
        showTitles: true,
        reservedSize: 35,
        getTitlesWidget: getVerticalRightTitlesWidget,
        interval: verticalTicksInterval,
      ),
    );

    final AxisTitles horizontalTitles = AxisTitles(
      sideTitles: SideTitles(
        showTitles: true,
        reservedSize: 20,
        getTitlesWidget: getHorizontalTitlesWidget,
      ),
    );

    return FlTitlesData(
      show: true,
      bottomTitles: horizontalTitles,
      leftTitles: verticalTitlesLeft,
      topTitles: none,
      rightTitles: verticalTitleRight,
    );
  }

  Widget getHorizontalTitlesWidget(double value, TitleMeta meta) {
    return const Text('');
  }

  Widget getVerticalLeftTitlesWidget(double value, TitleMeta meta) {
    return const Text('');
  }

  Widget getVerticalRightTitlesWidget(double value, TitleMeta meta) {
    return const Text('');
  }

  Widget getHorizontalTitlesWidgetWithDate(double value, TitleMeta meta) {
    final DateFormat formatter = DateFormat('dd/MM');

    final DateTime date = DateTime.fromMillisecondsSinceEpoch(value.toInt());
    final String text = formatter.format(date);

    return SideTitleWidget(
      meta: meta,
      space: 4,
      child: Padding(
        padding: const EdgeInsets.only(right: 10),
        child: RotationTransition(
          turns: const AlwaysStoppedAnimation(-30 / 360),
          child: Text(
            text,
            style: TextStyle(
              // color: getTextColor(),
              fontSize: titleFontSize,
            ),
          ),
        ),
      ),
    );
  }

  Widget getHorizontalTitlesWidgetWithDay(double value, TitleMeta meta) {
    final String dayShortName = getDayShortName(value.toInt());

    return SideTitleWidget(
      meta: meta,
      space: 2,
      child: Text(
        tr(dayShortName),
        style: const TextStyle(
          fontSize: 11,
        ),
      ),
    );
  }

  Widget getHorizontalTitlesWidgetWithHour(double value, TitleMeta meta) {
    String text = '';

    if (value % 4 == 0 || value == 23) {
      text = value.toInt().toString().padLeft(2, '0');
    }

    return SideTitleWidget(
      meta: meta,
      space: 2,
      child: Text(
        text,
        style: TextStyle(
          fontSize: titleFontSize,
        ),
      ),
    );
  }

  Widget getVerticalTitlesSpacerWidget(double value, TitleMeta meta) {
    return SideTitleWidget(
      meta: meta,
      space: 4,
      child: const Text(''),
    );
  }

  Widget getVerticalTitlesWidgetWithValue(double value, TitleMeta meta) {
    String suffix = verticalAxisTitleSuffix != '' ? ' $verticalAxisTitleSuffix' : '';

    return SideTitleWidget(
      meta: meta,
      space: 4,
      child: Text(
        value.toInt().toString() + suffix,
        style: TextStyle(
          fontSize: titleFontSize,
        ),
      ),
    );
  }
}
