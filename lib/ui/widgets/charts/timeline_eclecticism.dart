import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/config/app_colors.dart';
import 'package:scrobbles/models/data/timeline.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_line_chart.dart';

class ChartTimelineEclecticism extends CustomLineChart {
  const ChartTimelineEclecticism({super.key, required this.chartData});

  final TimelineData chartData;

  @override
  String get verticalAxisTitleSuffix => '%';

  @override
  Widget build(BuildContext context) {
    if (chartData.data.keys.isEmpty) {
      return SizedBox(
        height: chartHeight,
      );
    }

    final horizontalScale = getHorizontalScaleFromDates(chartData.data.keys);

    // Left/right margins: 12h, in milliseconds
    const double margin = 12 * 60 * 60 * 1000;

    return SizedBox(
      height: chartHeight,
      child: LineChart(
        LineChartData(
          lineBarsData: getDataEclecticism(),
          borderData: noBorderData,
          gridData: noGridData,
          titlesData: getTitlesData(),
          lineTouchData: const LineTouchData(enabled: false),
          minX: (horizontalScale['min'] ?? 0) - margin,
          maxX: (horizontalScale['max'] ?? 0) + margin,
          maxY: 100,
          minY: 0,
        ),
        duration: const Duration(milliseconds: 250),
      ),
    );
  }

  List<LineChartBarData> getDataEclecticism() {
    List<FlSpot> spots = [];

    for (var element in chartData.data.keys) {
      TimelineDataValue? value = chartData.data[element];
      if (value != null) {
        final double date = DateTime.parse(element).millisecondsSinceEpoch.toDouble();
        final double eclecticism = value.eclecticism.toDouble();

        spots.add(FlSpot(date, eclecticism));
      }
    }

    const baseColor = AppColors.contentColorCyan;
    final borderColor = baseColor.darken(20);

    return [
      LineChartBarData(
        isCurved: true,
        color: borderColor,
        barWidth: 2,
        isStrokeCapRound: false,
        dotData: const FlDotData(show: false),
        spots: spots,
      ),
    ];
  }

  @override
  Widget getVerticalRightTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesSpacerWidget(value, meta);
  }
}
