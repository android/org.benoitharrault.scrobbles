import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/models/data/topartists.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_chart.dart';

class ChartTopArtists extends CustomChart {
  const ChartTopArtists({super.key, required this.chartData});

  final TopArtistsData chartData;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 2.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: AspectRatio(
              aspectRatio: 1,
              child: PieChart(
                PieChartData(
                  sections: getPieChartData(),
                  sectionsSpace: 2,
                  centerSpaceRadius: 40,
                  startDegreeOffset: -45,
                  pieTouchData: PieTouchData(enabled: false),
                  borderData: FlBorderData(show: false),
                ),
              ),
            ),
          ),
          buildLegendWidget(),
        ],
      ),
    );
  }

  List<PieChartSectionData> getPieChartData() {
    List<PieChartSectionData> items = [];

    const radius = 40.0;
    const fontSize = 11.0;
    const shadows = [
      Shadow(
        color: Colors.black,
        blurRadius: 2,
      )
    ];

    int index = 0;

    for (var element in chartData.topArtists) {
      final Color baseColor = getColorFromIndex(index++);

      final PieChartSectionData item = PieChartSectionData(
        value: element.count.toDouble(),
        title: element.artistName,
        color: baseColor.darken(20),
        borderSide: BorderSide(
          color: baseColor.darken(40),
          width: 1,
        ),
        radius: radius,
        titleStyle: const TextStyle(
          fontSize: fontSize,
          shadows: shadows,
        ),
      );

      items.add(item);
    }

    return items;
  }

  Widget buildLegendWidget() {
    const double itemSize = 12;

    List<Widget> items = [];
    int index = 0;

    for (var element in chartData.topArtists) {
      final Color baseColor = getColorFromIndex(index++);

      final Widget item = Row(
        children: <Widget>[
          Container(
            width: itemSize,
            height: itemSize,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: baseColor.darken(20),
              border: Border.all(
                color: baseColor.darken(40),
                width: 1,
              ),
            ),
          ),
          const SizedBox(width: 4),
          Text(
            '${element.artistName} (${element.count})',
            style: const TextStyle(
              fontSize: itemSize - 2,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      );

      items.add(item);
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: items,
    );
  }
}
