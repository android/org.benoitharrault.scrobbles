import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/models/data/counts_by_day.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_bar_chart.dart';

class ChartCountsByDay extends CustomBarChart {
  const ChartCountsByDay({super.key, required this.chartData});

  final CountsByDayData chartData;

  @override
  double get verticalTicksInterval => 5;
  @override
  String get verticalAxisTitleSuffix => '%';

  @override
  Widget build(BuildContext context) {
    if (chartData.data.keys.isEmpty) {
      return SizedBox(
        height: chartHeight,
      );
    }

    return SizedBox(
      height: chartHeight,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final double maxWidth = constraints.maxWidth;
          final int barsCount = chartData.data.keys.length;

          return getBarChart(
            barWidth: getBarWidth(maxWidth, barsCount),
            backgroundColor: Theme.of(context).colorScheme.surface,
          );
        },
      ),
    );
  }

  @override
  double getMaxCountsValue() {
    double maxValue = 0;

    for (var key in chartData.data.keys) {
      double counts = chartData.data[key] ?? 0;
      if (counts > maxValue) {
        maxValue = counts;
      }
    }

    return maxValue;
  }

  Color getColorFromDayIndex(int dayIndex) {
    Color barColor = Colors.black;
    switch (dayIndex) {
      case 1:
        barColor = const Color.fromARGB(255, 255, 99, 132);
        break;
      case 2:
        barColor = const Color.fromARGB(255, 255, 159, 64);
        break;
      case 3:
        barColor = const Color.fromARGB(255, 255, 205, 86);
        break;
      case 4:
        barColor = const Color.fromARGB(255, 75, 192, 192);
        break;
      case 5:
        barColor = const Color.fromARGB(255, 54, 162, 235);
        break;
      case 6:
        barColor = const Color.fromARGB(255, 153, 102, 255);
        break;
      case 7:
        barColor = const Color.fromARGB(255, 201, 203, 207);
        break;
      default:
    }
    return barColor;
  }

  @override
  List<BarChartGroupData> getDataCounts(double barWidth) {
    List<BarChartGroupData> data = [];

    for (var day in chartData.data.keys) {
      final double? counts = chartData.data[day];

      if (counts != null) {
        final Color barColor = getColorFromDayIndex(day);

        data.add(getBarItem(
          x: day,
          values: [counts],
          barColors: [barColor],
          barWidth: barWidth,
        ));
      }
    }

    return data;
  }

  @override
  Widget getHorizontalTitlesWidget(double value, TitleMeta meta) {
    return getHorizontalTitlesWidgetWithDay(value, meta);
  }
}
