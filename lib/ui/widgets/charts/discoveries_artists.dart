import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/config/app_colors.dart';
import 'package:scrobbles/models/data/discoveries.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_bar_chart.dart';

class ChartDiscoveriesArtists extends CustomBarChart {
  const ChartDiscoveriesArtists({super.key, required this.chartData});

  final DiscoveriesData chartData;

  @override
  Widget build(BuildContext context) {
    if (chartData.data.keys.isEmpty) {
      return SizedBox(
        height: chartHeight,
      );
    }

    return SizedBox(
      height: chartHeight,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final double maxWidth = constraints.maxWidth;
          final int barsCount = chartData.data.keys.length;

          return getBarChart(
            barWidth: getBarWidth(maxWidth, barsCount),
            backgroundColor: Theme.of(context).colorScheme.surface,
          );
        },
      ),
    );
  }

  @override
  double getMaxCountsValue() {
    double maxValue = 0;

    for (var key in chartData.data.keys) {
      DiscoveriesDataValue? value = chartData.data[key];
      if (value != null) {
        double newArtistsCount = value.newArtistsCount.toDouble();

        if (newArtistsCount > maxValue) {
          maxValue = newArtistsCount;
        }
      }
    }

    return maxValue;
  }

  @override
  List<BarChartGroupData> getDataCounts(double barWidth) {
    List<BarChartGroupData> data = [];

    final newArtistsBarColor = AppColors.contentColorGreen.darken(20);

    for (var key in chartData.data.keys) {
      DiscoveriesDataValue? value = chartData.data[key];
      if (value != null) {
        final int date = DateTime.parse(key).millisecondsSinceEpoch;

        data.add(getBarItem(
          x: date,
          values: [
            value.newArtistsCount.toDouble(),
          ],
          barColors: [
            newArtistsBarColor,
          ],
          barWidth: barWidth,
        ));
      }
    }

    return data;
  }
}
