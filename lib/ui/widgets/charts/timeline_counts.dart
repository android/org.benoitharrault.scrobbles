import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/config/app_colors.dart';
import 'package:scrobbles/models/data/timeline.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_bar_chart.dart';

class ChartTimelineCounts extends CustomBarChart {
  const ChartTimelineCounts({super.key, required this.chartData});

  final TimelineData chartData;

  @override
  double get verticalTicksInterval => 50;

  @override
  Widget build(BuildContext context) {
    if (chartData.data.keys.isEmpty) {
      return SizedBox(
        height: chartHeight,
      );
    }

    return SizedBox(
      height: chartHeight,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final double maxWidth = constraints.maxWidth;
          final int barsCount = chartData.data.keys.length;

          return getBarChart(
            barWidth: getBarWidth(maxWidth, barsCount),
            backgroundColor: Theme.of(context).colorScheme.surface,
          );
        },
      ),
    );
  }

  @override
  double getMaxCountsValue() {
    double maxValue = 0;

    for (var key in chartData.data.keys) {
      TimelineDataValue? value = chartData.data[key];
      if (value != null) {
        double counts = value.counts.toDouble();
        if (counts > maxValue) {
          maxValue = counts;
        }
      }
    }

    return maxValue;
  }

  @override
  List<BarChartGroupData> getDataCounts(double barWidth) {
    List<BarChartGroupData> data = [];

    const barColor = AppColors.contentColorOrange;

    for (var key in chartData.data.keys) {
      TimelineDataValue? value = chartData.data[key];
      if (value != null) {
        final int date = DateTime.parse(key).millisecondsSinceEpoch;
        final double counts = value.counts.toDouble();

        data.add(getBarItem(
          x: date,
          values: [counts],
          barColors: [barColor],
          barWidth: barWidth,
        ));
      }
    }

    return data;
  }

  @override
  Widget getVerticalLeftTitlesWidget(double value, TitleMeta meta) {
    return getVerticalTitlesSpacerWidget(value, meta);
  }
}
