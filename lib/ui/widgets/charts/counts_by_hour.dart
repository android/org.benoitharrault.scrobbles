import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:fl_chart/fl_chart.dart';

import 'package:scrobbles/config/app_colors.dart';
import 'package:scrobbles/models/data/counts_by_hour.dart';
import 'package:scrobbles/ui/widgets/abstracts/custom_bar_chart.dart';

class ChartCountsByHour extends CustomBarChart {
  const ChartCountsByHour({
    super.key,
    required this.chartData,
  });

  final CountsByHourData chartData;

  @override
  double get verticalTicksInterval => 5;
  @override
  String get verticalAxisTitleSuffix => '%';

  @override
  Widget build(BuildContext context) {
    if (chartData.data.keys.isEmpty) {
      return SizedBox(
        height: chartHeight,
      );
    }

    return SizedBox(
      height: chartHeight,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final double maxWidth = constraints.maxWidth;
          final int barsCount = chartData.data.keys.length;

          return getBarChart(
            barWidth: getBarWidth(maxWidth, barsCount),
            backgroundColor: Theme.of(context).colorScheme.surface,
          );
        },
      ),
    );
  }

  @override
  double getMaxCountsValue() {
    double maxValue = 0;

    for (var key in chartData.data.keys) {
      double counts = chartData.data[key] ?? 0;
      if (counts > maxValue) {
        maxValue = counts;
      }
    }

    return maxValue;
  }

  @override
  List<BarChartGroupData> getDataCounts(double barWidth) {
    List<BarChartGroupData> data = [];

    final barColor = AppColors.contentColorCyan.darken(30);

    for (var day in chartData.data.keys) {
      final double? counts = chartData.data[day];

      if (counts != null) {
        data.add(getBarItem(
          x: day,
          values: [counts],
          barColors: [barColor],
          barWidth: barWidth,
        ));
      }
    }

    return data;
  }

  @override
  Widget getHorizontalTitlesWidget(double value, TitleMeta meta) {
    return getHorizontalTitlesWidgetWithHour(value, meta);
  }
}
