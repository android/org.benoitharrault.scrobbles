import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_discoveries_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_artists_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_tracks_cubit.dart';
import 'package:scrobbles/ui/widgets/cards/discoveries.dart';
import 'package:scrobbles/ui/widgets/cards/new_artists.dart';
import 'package:scrobbles/ui/widgets/cards/new_tracks.dart';

class PageDiscoveries extends StatelessWidget {
  const PageDiscoveries({super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<DataDiscoveriesCubit>(context).refresh(context);
        BlocProvider.of<DataNewArtistsCubit>(context).refresh(context);
        BlocProvider.of<DataNewTracksCubit>(context).refresh(context);
      },
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        physics: const BouncingScrollPhysics(),
        children: const <Widget>[
          SizedBox(height: 8),
          CardDiscoveries(),
          SizedBox(height: 6),
          CardNewArtists(),
          SizedBox(height: 6),
          CardNewTracks(),
          SizedBox(height: 36),
        ],
      ),
    );
  }
}
