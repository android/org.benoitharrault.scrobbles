import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_counts_by_day_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_hour_cubit.dart';
import 'package:scrobbles/cubit/activity/data_heatmap_cubit.dart';
import 'package:scrobbles/ui/widgets/cards/counts_by_day.dart';
import 'package:scrobbles/ui/widgets/cards/counts_by_hour.dart';
import 'package:scrobbles/ui/widgets/cards/heatmap.dart';

class PageStatistics extends StatelessWidget {
  const PageStatistics({super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<DataCountsByDayCubit>(context).refresh(context);
        BlocProvider.of<DataCountsByHourCubit>(context).refresh(context);
        BlocProvider.of<DataHeatmapCubit>(context).refresh(context);
      },
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        physics: const BouncingScrollPhysics(),
        children: const <Widget>[
          SizedBox(height: 8),
          CardHeatmap(),
          SizedBox(height: 6),
          CardCountsByDay(),
          SizedBox(height: 6),
          CardCountsByHour(),
          SizedBox(height: 70),
        ],
      ),
    );
  }
}
