import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/cubit/activity/data_statistics_global_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_recent_cubit.dart';
import 'package:scrobbles/cubit/activity/data_timeline_cubit.dart';
import 'package:scrobbles/cubit/activity/data_top_artists_cubit.dart';
import 'package:scrobbles/ui/widgets/cards/statistics_global.dart';
import 'package:scrobbles/ui/widgets/cards/statistics_recent.dart';
import 'package:scrobbles/ui/widgets/cards/timeline.dart';
import 'package:scrobbles/ui/widgets/cards/top_artists.dart';

class PageHome extends StatelessWidget {
  const PageHome({super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<DataStatisticsGlobalCubit>(context).refresh(context);
        BlocProvider.of<DataStatisticsRecentCubit>(context).refresh(context);
        BlocProvider.of<DataTimelineCubit>(context).refresh(context);
        BlocProvider.of<DataTopArtistsCubit>(context).refresh(context);
      },
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        physics: const BouncingScrollPhysics(),
        children: const <Widget>[
          SizedBox(height: 8),
          CardStatisticsGlobal(),
          SizedBox(height: 6),
          CardStatisticsRecent(),
          SizedBox(height: 6),
          CardTimeline(),
          SizedBox(height: 6),
          CardTopArtists(),
          SizedBox(height: 36),
        ],
      ),
    );
  }
}
