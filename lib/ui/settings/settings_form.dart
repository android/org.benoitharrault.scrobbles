import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_day_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_hour_cubit.dart';
import 'package:scrobbles/cubit/activity/data_discoveries_cubit.dart';
import 'package:scrobbles/cubit/activity/data_heatmap_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_artists_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_tracks_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_recent_cubit.dart';
import 'package:scrobbles/cubit/activity/data_timeline_cubit.dart';
import 'package:scrobbles/cubit/activity/data_top_artists_cubit.dart';

import 'package:scrobbles/config/application_config.dart';

class SettingsForm extends StatefulWidget {
  const SettingsForm({super.key});

  @override
  State<SettingsForm> createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  String discoveriesDaysCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeDiscoveriesDaysCount)
      .defaultValue;
  String distributionDaysCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeDistributionDaysCount)
      .defaultValue;
  String statisticsRecentDaysCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeStatisticsRecentDaysCount)
      .defaultValue;
  String timelineDaysCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeTimelineDaysCount)
      .defaultValue;
  String topArtistsDaysCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeTopArtistsDaysCount)
      .defaultValue;
  String newArtistsCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeNewArtistsCount)
      .defaultValue;
  String newTracksCount = ApplicationConfig.config
      .getFromCode(ApplicationConfig.parameterCodeNewTracksCount)
      .defaultValue;

  final List<bool> _selectedDiscoveriesDaysCount = [];
  final List<bool> _selectedDistributionDaysCount = [];
  final List<bool> _selectedStatisticsRecentDaysCount = [];
  final List<bool> _selectedTimelineDaysCount = [];
  final List<bool> _selectedTopArtistsDaysCount = [];
  final List<bool> _selectedNewArtistsCount = [];
  final List<bool> _selectedNewTracksCount = [];

  @override
  void didChangeDependencies() {
    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;

    discoveriesDaysCount = settings.get(ApplicationConfig.parameterCodeDiscoveriesDaysCount);
    distributionDaysCount = settings.get(ApplicationConfig.parameterCodeDistributionDaysCount);
    statisticsRecentDaysCount =
        settings.get(ApplicationConfig.parameterCodeStatisticsRecentDaysCount);
    timelineDaysCount = settings.get(ApplicationConfig.parameterCodeTimelineDaysCount);
    topArtistsDaysCount = settings.get(ApplicationConfig.parameterCodeTopArtistsDaysCount);
    newArtistsCount = settings.get(ApplicationConfig.parameterCodeNewArtistsCount);
    newTracksCount = settings.get(ApplicationConfig.parameterCodeNewTracksCount);

    _selectedDiscoveriesDaysCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedDaysCountValues) {
      _selectedDiscoveriesDaysCount.add(parameterItemValue.value == discoveriesDaysCount);
    }

    _selectedDistributionDaysCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedDaysCountValues) {
      _selectedDistributionDaysCount.add(parameterItemValue.value == distributionDaysCount);
    }

    _selectedStatisticsRecentDaysCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedDaysCountValues) {
      _selectedStatisticsRecentDaysCount
          .add(parameterItemValue.value == statisticsRecentDaysCount);
    }

    _selectedTimelineDaysCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedDaysCountValues) {
      _selectedTimelineDaysCount.add(parameterItemValue.value == timelineDaysCount);
    }

    _selectedTopArtistsDaysCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedDaysCountValues) {
      _selectedTopArtistsDaysCount.add(parameterItemValue.value == topArtistsDaysCount);
    }

    _selectedNewArtistsCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedCountValues) {
      _selectedNewArtistsCount.add(parameterItemValue.value == newArtistsCount);
    }

    _selectedNewTracksCount.clear();
    for (var parameterItemValue in ApplicationConfig.allowedCountValues) {
      _selectedNewTracksCount.add(parameterItemValue.value == newTracksCount);
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    void saveSettings() {
      BlocProvider.of<ActivitySettingsCubit>(context).setValues(values: {
        ApplicationConfig.parameterCodeDiscoveriesDaysCount: discoveriesDaysCount,
        ApplicationConfig.parameterCodeDistributionDaysCount: distributionDaysCount,
        ApplicationConfig.parameterCodeStatisticsRecentDaysCount: statisticsRecentDaysCount,
        ApplicationConfig.parameterCodeTimelineDaysCount: timelineDaysCount,
        ApplicationConfig.parameterCodeTopArtistsDaysCount: topArtistsDaysCount,
        ApplicationConfig.parameterCodeNewArtistsCount: newArtistsCount,
        ApplicationConfig.parameterCodeNewTracksCount: newTracksCount,
      });
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        const SizedBox(height: 8),

        // Light/dark theme
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Text('settings_label_theme').tr(),
            const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.system,
                  icon: UniconsLine.cog,
                ),
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.light,
                  icon: UniconsLine.sun,
                ),
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.dark,
                  icon: UniconsLine.moon,
                )
              ],
            ),
          ],
        ),

        const SizedBox(height: 16),

        AppTitle(text: tr('settings_title_days_count')),

        // Statistics (recent)
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_statistics_recent_days_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  statisticsRecentDaysCount =
                      ApplicationConfig.allowedDaysCountValues[index].value;
                  for (int i = 0; i < _selectedStatisticsRecentDaysCount.length; i++) {
                    _selectedStatisticsRecentDaysCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataStatisticsRecentCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedStatisticsRecentDaysCount,
              children: ApplicationConfig.allowedDaysCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        // Timeline
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_timeline_days_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  timelineDaysCount = ApplicationConfig.allowedDaysCountValues[index].value;
                  for (int i = 0; i < _selectedTimelineDaysCount.length; i++) {
                    _selectedTimelineDaysCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataTimelineCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedTimelineDaysCount,
              children: ApplicationConfig.allowedDaysCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        // Top Artists
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_top_artists_days_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  topArtistsDaysCount = ApplicationConfig.allowedDaysCountValues[index].value;
                  for (int i = 0; i < _selectedTopArtistsDaysCount.length; i++) {
                    _selectedTopArtistsDaysCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataTopArtistsCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedTopArtistsDaysCount,
              children: ApplicationConfig.allowedDaysCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        // Discoveries
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_discoveries_days_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  discoveriesDaysCount = ApplicationConfig.allowedDaysCountValues[index].value;
                  for (int i = 0; i < _selectedDiscoveriesDaysCount.length; i++) {
                    _selectedDiscoveriesDaysCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataDiscoveriesCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedDiscoveriesDaysCount,
              children: ApplicationConfig.allowedDaysCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        // Distribution by day/hour + heatmap
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_distribution_days_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  distributionDaysCount =
                      ApplicationConfig.allowedDaysCountValues[index].value;
                  for (int i = 0; i < _selectedDistributionDaysCount.length; i++) {
                    _selectedDistributionDaysCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataCountsByDayCubit>(context).refresh(context);
                BlocProvider.of<DataCountsByHourCubit>(context).refresh(context);
                BlocProvider.of<DataHeatmapCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedDistributionDaysCount,
              children: ApplicationConfig.allowedDaysCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        const SizedBox(height: 8),
        AppTitle(text: tr('settings_title_counts')),

        // New artists count
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_new_artists_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  newArtistsCount = ApplicationConfig.allowedCountValues[index].value;
                  for (int i = 0; i < _selectedNewArtistsCount.length; i++) {
                    _selectedNewArtistsCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataNewArtistsCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedNewArtistsCount,
              children: ApplicationConfig.allowedCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),

        // New tracks count
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text('settings_label_new_tracks_count').tr(),
            ToggleButtons(
              onPressed: (int index) {
                setState(() {
                  newTracksCount = ApplicationConfig.allowedCountValues[index].value;
                  for (int i = 0; i < _selectedNewTracksCount.length; i++) {
                    _selectedNewTracksCount[i] = i == index;
                  }
                });
                saveSettings();
                BlocProvider.of<DataNewTracksCubit>(context).refresh(context);
              },
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              constraints: const BoxConstraints(minHeight: 30.0, minWidth: 30.0),
              isSelected: _selectedNewTracksCount,
              children: ApplicationConfig.allowedCountValues
                  .map((ApplicationSettingsParameterItemValue parameterItemValue) =>
                      Text(parameterItemValue.toString()))
                  .toList(),
            ),
          ],
        ),
      ],
    );
  }
}
