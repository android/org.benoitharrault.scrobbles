part of 'data_counts_by_day_cubit.dart';

@immutable
class DataCountsByDayState extends Equatable {
  const DataCountsByDayState({
    required this.countsByDay,
    required this.status,
  });

  final CountsByDayData? countsByDay;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        countsByDay,
        status,
      ];
}
