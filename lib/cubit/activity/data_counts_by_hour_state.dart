part of 'data_counts_by_hour_cubit.dart';

@immutable
class DataCountsByHourState extends Equatable {
  const DataCountsByHourState({
    required this.countsByHour,
    required this.status,
  });

  final CountsByHourData? countsByHour;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        countsByHour,
        status,
      ];
}
