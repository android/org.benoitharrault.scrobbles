import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/counts_by_hour.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_counts_by_hour_state.dart';

class DataCountsByHourCubit extends HydratedCubit<DataCountsByHourState> {
  DataCountsByHourCubit()
      : super(const DataCountsByHourState(
          countsByHour: null,
          status: '',
        ));

  void setLoading() {
    emit(DataCountsByHourState(
      countsByHour: state.countsByHour,
      status: 'loading',
    ));
  }

  void setValue(CountsByHourData? countsByHour) {
    emit(DataCountsByHourState(
      countsByHour: countsByHour,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataCountsByHourState(
      countsByHour: state.countsByHour,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataCountsByHourState(
      countsByHour: state.countsByHour,
      status: '',
    ));
  }

  void update(CountsByHourData? countsByHour) {
    if ((countsByHour != null) && (state.countsByHour.toString() != countsByHour.toString())) {
      setValue(countsByHour);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    final CountsByHourData? data = await ScrobblesApi.fetchCountsByHour(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataCountsByHourState? fromJson(Map<String, dynamic> json) {
    return DataCountsByHourState(
      countsByHour: CountsByHourData.fromJson(json['countsByHour']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataCountsByHourState state) {
    return <String, Object?>{
      'countsByHour': state.countsByHour?.toJson(),
      'status': state.status,
    };
  }
}
