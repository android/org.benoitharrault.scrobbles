import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/discoveries.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_discoveries_state.dart';

class DataDiscoveriesCubit extends HydratedCubit<DataDiscoveriesState> {
  DataDiscoveriesCubit()
      : super(const DataDiscoveriesState(
          discoveries: null,
          status: '',
        ));

  void setLoading() {
    emit(DataDiscoveriesState(
      discoveries: state.discoveries,
      status: 'loading',
    ));
  }

  void setValue(DiscoveriesData? discoveries) {
    emit(DataDiscoveriesState(
      discoveries: discoveries,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataDiscoveriesState(
      discoveries: state.discoveries,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataDiscoveriesState(
      discoveries: state.discoveries,
      status: '',
    ));
  }

  void update(DiscoveriesData? discoveries) {
    if ((discoveries != null) && (state.discoveries.toString() != discoveries.toString())) {
      setValue(discoveries);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDiscoveriesDaysCount);

    final DiscoveriesData? data = await ScrobblesApi.fetchDiscoveries(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataDiscoveriesState? fromJson(Map<String, dynamic> json) {
    return DataDiscoveriesState(
      discoveries: DiscoveriesData.fromJson(json['discoveries']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataDiscoveriesState state) {
    return <String, Object?>{
      'discoveries': state.discoveries?.toJson(),
      'status': state.status,
    };
  }
}
