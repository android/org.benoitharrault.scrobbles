import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/timeline.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_timeline_state.dart';

class DataTimelineCubit extends HydratedCubit<DataTimelineState> {
  DataTimelineCubit()
      : super(const DataTimelineState(
          timeline: null,
          status: '',
        ));

  void setLoading() {
    emit(DataTimelineState(
      timeline: state.timeline,
      status: 'loading',
    ));
  }

  void setValue(TimelineData? timeline) {
    emit(DataTimelineState(
      timeline: timeline,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataTimelineState(
      timeline: state.timeline,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataTimelineState(
      timeline: state.timeline,
      status: '',
    ));
  }

  void update(TimelineData? timeline) {
    if ((timeline != null) && (state.timeline.toString() != timeline.toString())) {
      setValue(timeline);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount = settings.getAsInt(ApplicationConfig.parameterCodeTimelineDaysCount);

    final TimelineData? data = await ScrobblesApi.fetchTimeline(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataTimelineState? fromJson(Map<String, dynamic> json) {
    return DataTimelineState(
      timeline: TimelineData.fromJson(json['timeline']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataTimelineState state) {
    return <String, Object?>{
      'timeline': state.timeline?.toJson(),
      'status': state.status,
    };
  }
}
