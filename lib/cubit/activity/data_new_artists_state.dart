part of 'data_new_artists_cubit.dart';

@immutable
class DataNewArtistsState extends Equatable {
  const DataNewArtistsState({
    required this.newArtists,
    required this.status,
  });

  final NewArtistsData? newArtists;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        newArtists,
        status,
      ];
}
