part of 'data_discoveries_cubit.dart';

@immutable
class DataDiscoveriesState extends Equatable {
  const DataDiscoveriesState({
    required this.discoveries,
    required this.status,
  });

  final DiscoveriesData? discoveries;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        discoveries,
        status,
      ];
}
