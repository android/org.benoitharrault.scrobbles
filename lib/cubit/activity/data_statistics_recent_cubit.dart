import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/statistics_recent.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_statistics_recent_state.dart';

class DataStatisticsRecentCubit extends HydratedCubit<DataStatisticsRecentState> {
  DataStatisticsRecentCubit()
      : super(const DataStatisticsRecentState(
          statisticsRecent: null,
          status: '',
        ));

  void setLoading() {
    emit(DataStatisticsRecentState(
      statisticsRecent: state.statisticsRecent,
      status: 'loading',
    ));
  }

  void setValue(StatisticsRecentData? statisticsRecent) {
    emit(DataStatisticsRecentState(
      statisticsRecent: statisticsRecent,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataStatisticsRecentState(
      statisticsRecent: state.statisticsRecent,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataStatisticsRecentState(
      statisticsRecent: state.statisticsRecent,
      status: '',
    ));
  }

  void update(StatisticsRecentData? statisticsRecent) {
    if ((statisticsRecent != null) &&
        (state.statisticsRecent.toString() != statisticsRecent.toString())) {
      setValue(statisticsRecent);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeStatisticsRecentDaysCount);

    final StatisticsRecentData? data = await ScrobblesApi.fetchRecentStatistics(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataStatisticsRecentState? fromJson(Map<String, dynamic> json) {
    return DataStatisticsRecentState(
      statisticsRecent: StatisticsRecentData.fromJson(json['statisticsRecent']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataStatisticsRecentState state) {
    return <String, Object?>{
      'statisticsRecent': state.statisticsRecent?.toJson(),
      'status': state.status,
    };
  }
}
