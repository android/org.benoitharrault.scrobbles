part of 'data_statistics_recent_cubit.dart';

@immutable
class DataStatisticsRecentState extends Equatable {
  const DataStatisticsRecentState({
    required this.statisticsRecent,
    required this.status,
  });

  final StatisticsRecentData? statisticsRecent;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        statisticsRecent,
        status,
      ];
}
