part of 'data_new_tracks_cubit.dart';

@immutable
class DataNewTracksState extends Equatable {
  const DataNewTracksState({
    required this.newTracks,
    required this.status,
  });

  final NewTracksData? newTracks;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        newTracks,
        status,
      ];
}
