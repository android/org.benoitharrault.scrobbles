import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/new_artists.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_new_artists_state.dart';

class DataNewArtistsCubit extends HydratedCubit<DataNewArtistsState> {
  DataNewArtistsCubit()
      : super(const DataNewArtistsState(
          newArtists: null,
          status: '',
        ));

  void setLoading() {
    emit(DataNewArtistsState(
      newArtists: state.newArtists,
      status: 'loading',
    ));
  }

  void setValue(NewArtistsData? newArtists) {
    emit(DataNewArtistsState(
      newArtists: newArtists,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataNewArtistsState(
      newArtists: state.newArtists,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataNewArtistsState(
      newArtists: state.newArtists,
      status: '',
    ));
  }

  void update(NewArtistsData? newArtists) {
    if ((newArtists != null) && (state.newArtists.toString() != newArtists.toString())) {
      setValue(newArtists);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int count = settings.getAsInt(ApplicationConfig.parameterCodeNewArtistsCount);

    final NewArtistsData? data = await ScrobblesApi.fetchNewArtists(count);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataNewArtistsState? fromJson(Map<String, dynamic> json) {
    return DataNewArtistsState(
      newArtists: NewArtistsData.fromJson(json['newArtists']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataNewArtistsState state) {
    return <String, Object?>{
      'newArtists': state.newArtists?.toJson(),
      'status': state.status,
    };
  }
}
