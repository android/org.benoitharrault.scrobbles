part of 'data_top_artists_cubit.dart';

@immutable
class DataTopArtistsState extends Equatable {
  const DataTopArtistsState({
    required this.topArtists,
    required this.status,
  });

  final TopArtistsData? topArtists;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        topArtists,
        status,
      ];
}
