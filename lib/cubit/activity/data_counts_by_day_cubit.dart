import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/counts_by_day.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_counts_by_day_state.dart';

class DataCountsByDayCubit extends HydratedCubit<DataCountsByDayState> {
  DataCountsByDayCubit()
      : super(const DataCountsByDayState(
          countsByDay: null,
          status: '',
        ));

  void setLoading() {
    emit(DataCountsByDayState(
      countsByDay: state.countsByDay,
      status: 'loading',
    ));
  }

  void setValue(CountsByDayData? countsByDay) {
    emit(DataCountsByDayState(
      countsByDay: countsByDay,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataCountsByDayState(
      countsByDay: state.countsByDay,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataCountsByDayState(
      countsByDay: state.countsByDay,
      status: '',
    ));
  }

  void update(CountsByDayData? countsByDay) {
    if ((countsByDay != null) && (state.countsByDay.toString() != countsByDay.toString())) {
      setValue(countsByDay);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    final CountsByDayData? data = await ScrobblesApi.fetchCountsByDay(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataCountsByDayState? fromJson(Map<String, dynamic> json) {
    return DataCountsByDayState(
      countsByDay: CountsByDayData.fromJson(json['countsByDay']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataCountsByDayState state) {
    return <String, Object?>{
      'countsByDay': state.countsByDay?.toJson(),
      'status': state.status,
    };
  }
}
