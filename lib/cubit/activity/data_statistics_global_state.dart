part of 'data_statistics_global_cubit.dart';

@immutable
class DataStatisticsGlobalState extends Equatable {
  const DataStatisticsGlobalState({
    required this.statisticsGlobal,
    required this.status,
  });

  final StatisticsGlobalData? statisticsGlobal;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        statisticsGlobal,
        status,
      ];
}
