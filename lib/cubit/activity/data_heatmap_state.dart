part of 'data_heatmap_cubit.dart';

@immutable
class DataHeatmapState extends Equatable {
  const DataHeatmapState({
    required this.heatmap,
    required this.status,
  });

  final HeatmapData? heatmap;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        heatmap,
        status,
      ];
}
