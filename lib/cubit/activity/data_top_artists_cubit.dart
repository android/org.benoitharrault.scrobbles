import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/topartists.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_top_artists_state.dart';

class DataTopArtistsCubit extends HydratedCubit<DataTopArtistsState> {
  DataTopArtistsCubit()
      : super(const DataTopArtistsState(
          topArtists: null,
          status: '',
        ));

  void setLoading() {
    emit(DataTopArtistsState(
      topArtists: state.topArtists,
      status: 'loading',
    ));
  }

  void setValue(TopArtistsData? topArtists) {
    emit(DataTopArtistsState(
      topArtists: topArtists,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataTopArtistsState(
      topArtists: state.topArtists,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataTopArtistsState(
      topArtists: state.topArtists,
      status: '',
    ));
  }

  void update(TopArtistsData? topArtists) {
    if ((topArtists != null) && (state.topArtists.toString() != topArtists.toString())) {
      setValue(topArtists);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeTopArtistsDaysCount);

    final TopArtistsData? data = await ScrobblesApi.fetchTopArtists(daysCount);
    if (data != null) {
      if (context.mounted) update(data);
    } else {
      if (context.mounted) setFailed();
    }
  }

  @override
  DataTopArtistsState? fromJson(Map<String, dynamic> json) {
    return DataTopArtistsState(
      topArtists: TopArtistsData.fromJson(json['topArtists']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataTopArtistsState state) {
    return <String, Object?>{
      'topArtists': state.topArtists?.toJson(),
      'status': state.status,
    };
  }
}
