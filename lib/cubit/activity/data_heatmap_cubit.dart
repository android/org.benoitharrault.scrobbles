import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/heatmap.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_heatmap_state.dart';

class DataHeatmapCubit extends HydratedCubit<DataHeatmapState> {
  DataHeatmapCubit()
      : super(const DataHeatmapState(
          heatmap: null,
          status: '',
        ));

  void setLoading() {
    emit(DataHeatmapState(
      heatmap: state.heatmap,
      status: 'loading',
    ));
  }

  void setValue(HeatmapData? heatmapData) {
    emit(DataHeatmapState(
      heatmap: heatmapData,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataHeatmapState(
      heatmap: state.heatmap,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataHeatmapState(
      heatmap: state.heatmap,
      status: '',
    ));
  }

  void update(HeatmapData? heatmapData) {
    if ((heatmapData != null) && (state.heatmap.toString() != heatmapData.toString())) {
      setValue(heatmapData);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int daysCount =
        settings.getAsInt(ApplicationConfig.parameterCodeDistributionDaysCount);

    final HeatmapData? data = await ScrobblesApi.fetchHeatmap(daysCount);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataHeatmapState? fromJson(Map<String, dynamic> json) {
    return DataHeatmapState(
      heatmap: HeatmapData.fromJson(json['heatmap']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataHeatmapState state) {
    return <String, Object?>{
      'heatmap': state.heatmap?.toJson(),
      'status': state.status,
    };
  }
}
