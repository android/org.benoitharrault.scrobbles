import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/models/data/new_tracks.dart';
import 'package:scrobbles/network/scrobbles.dart';

part 'data_new_tracks_state.dart';

class DataNewTracksCubit extends HydratedCubit<DataNewTracksState> {
  DataNewTracksCubit()
      : super(const DataNewTracksState(
          newTracks: null,
          status: '',
        ));

  void setLoading() {
    emit(DataNewTracksState(
      newTracks: state.newTracks,
      status: 'loading',
    ));
  }

  void setValue(NewTracksData? newTracks) {
    emit(DataNewTracksState(
      newTracks: newTracks,
      status: '',
    ));
  }

  void setFailed() {
    emit(DataNewTracksState(
      newTracks: state.newTracks,
      status: 'failed',
    ));
  }

  void setLoaded() {
    emit(DataNewTracksState(
      newTracks: state.newTracks,
      status: '',
    ));
  }

  void update(NewTracksData? newTracks) {
    if ((newTracks != null) && (state.newTracks.toString() != newTracks.toString())) {
      setValue(newTracks);
    } else {
      setLoaded();
    }
  }

  void refresh(BuildContext context) async {
    setLoading();

    ActivitySettings settings = BlocProvider.of<ActivitySettingsCubit>(context).state.settings;
    final int count = settings.getAsInt(ApplicationConfig.parameterCodeNewTracksCount);

    final NewTracksData? data = await ScrobblesApi.fetchNewTracks(count);
    if (data != null) {
      update(data);
    } else {
      setFailed();
    }
  }

  @override
  DataNewTracksState? fromJson(Map<String, dynamic> json) {
    return DataNewTracksState(
      newTracks: NewTracksData.fromJson(json['newTracks']),
      status: '',
    );
  }

  @override
  Map<String, Object?>? toJson(DataNewTracksState state) {
    return <String, Object?>{
      'newTracks': state.newTracks?.toJson(),
      'status': state.status,
    };
  }
}
