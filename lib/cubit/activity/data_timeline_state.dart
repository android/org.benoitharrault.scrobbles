part of 'data_timeline_cubit.dart';

@immutable
class DataTimelineState extends Equatable {
  const DataTimelineState({
    required this.timeline,
    required this.status,
  });

  final TimelineData? timeline;
  final String status;

  @override
  List<Object?> get props => <Object?>[
        timeline,
        status,
      ];
}
