import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:scrobbles/ui/settings/settings_form.dart';

import 'package:scrobbles/ui/pages/discoveries.dart';
import 'package:scrobbles/ui/pages/home.dart';
import 'package:scrobbles/ui/pages/statistics.dart';

import 'package:scrobbles/cubit/activity/activity_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_day_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_hour_cubit.dart';
import 'package:scrobbles/cubit/activity/data_discoveries_cubit.dart';
import 'package:scrobbles/cubit/activity/data_heatmap_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_artists_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_tracks_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_global_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_recent_cubit.dart';
import 'package:scrobbles/cubit/activity/data_timeline_cubit.dart';
import 'package:scrobbles/cubit/activity/data_top_artists_cubit.dart';

class ApplicationConfig {
  static const String parameterCodeDistributionDaysCount = 'distribution-days-count';
  static const String parameterCodeDiscoveriesDaysCount = 'discoveries-days-count';
  static const String parameterCodeNewArtistsCount = 'new-artists-count';
  static const String parameterCodeNewTracksCount = 'new-tracks-count';
  static const String parameterCodeTimelineDaysCount = 'timeline-days-count';
  static const String parameterCodeTopArtistsDaysCount = 'top-artists-days-count';
  static const String parameterCodeStatisticsRecentDaysCount = 'statistics-recent-days-count';

  static const List<ApplicationSettingsParameterItemValue> allowedDaysCountValues = [
    ApplicationSettingsParameterItemValue(
      value: '7',
    ),
    ApplicationSettingsParameterItemValue(
      value: '14',
      isDefault: true,
    ),
    ApplicationSettingsParameterItemValue(
      value: '21',
    ),
    ApplicationSettingsParameterItemValue(
      value: '30',
    ),
    ApplicationSettingsParameterItemValue(
      value: '60',
    ),
    ApplicationSettingsParameterItemValue(
      value: '90',
    ),
  ];

  static const List<ApplicationSettingsParameterItemValue> allowedCountValues = [
    ApplicationSettingsParameterItemValue(
      value: '5',
      isDefault: true,
    ),
    ApplicationSettingsParameterItemValue(
      value: '10',
    ),
    ApplicationSettingsParameterItemValue(
      value: '20',
    ),
  ];

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexDiscoveries = 1;
  static const int activityPageIndexStatistics = 2;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Scrobbles',
    activitySettings: [
      // DistributionDaysCount
      ApplicationSettingsParameter(
        code: parameterCodeDistributionDaysCount,
        values: allowedDaysCountValues,
      ),

      // DiscoveriesDaysCount
      ApplicationSettingsParameter(
        code: parameterCodeDiscoveriesDaysCount,
        values: allowedDaysCountValues,
      ),

      // NewArtistsCount
      ApplicationSettingsParameter(
        code: parameterCodeNewArtistsCount,
        values: allowedCountValues,
      ),

      // NewTracksCount
      ApplicationSettingsParameter(
        code: parameterCodeNewTracksCount,
        values: allowedCountValues,
      ),

      // TimelineDaysCount
      ApplicationSettingsParameter(
        code: parameterCodeTimelineDaysCount,
        values: allowedDaysCountValues,
      ),

      // TopArtistsDaysCount
      ApplicationSettingsParameter(
        code: parameterCodeTopArtistsDaysCount,
        values: allowedDaysCountValues,
      ),

      // StatisticsRecentDaysCount
      ApplicationSettingsParameter(
        code: parameterCodeStatisticsRecentDaysCount,
        values: allowedDaysCountValues,
      ),
    ],
    autoStartActivity: true,
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context).updateIndex(activityPageIndexHome);
    },
    quitCurrentActivity: (BuildContext context) {},
    deleteCurrentActivity: (BuildContext context) {},
    resumeActivity: (BuildContext context) {},
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => SettingsForm(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      appBarConfiguration: AppBarConfiguration(
        hideQuitActivityButton: true,
        topBarButtonsBuilder: (context) => [
          // Go to Settings page
          AppBarButton(
            onPressed: (BuildContext context) =>
                BlocProvider.of<NavCubitScreen>(context).goToScreenSettings(),
            icon: ApplicationConfig.config.navigation.screenSettings.icon,
          ),

          // Back to Home page
          AppBarButton(
            onPressed: (BuildContext context) =>
                BlocProvider.of<NavCubitScreen>(context).goToScreenActivity(),
            icon: ApplicationConfig.config.navigation.screenActivity.icon,
          ),

          // Refresh data
          AppBarButton(
            onPressed: (BuildContext context) {
              BlocProvider.of<DataCountsByDayCubit>(context).refresh(context);
              BlocProvider.of<DataCountsByHourCubit>(context).refresh(context);
              BlocProvider.of<DataDiscoveriesCubit>(context).refresh(context);
              BlocProvider.of<DataHeatmapCubit>(context).refresh(context);
              BlocProvider.of<DataNewArtistsCubit>(context).refresh(context);
              BlocProvider.of<DataNewTracksCubit>(context).refresh(context);
              BlocProvider.of<DataStatisticsGlobalCubit>(context).refresh(context);
              BlocProvider.of<DataStatisticsRecentCubit>(context).refresh(context);
              BlocProvider.of<DataTimelineCubit>(context).refresh(context);
              BlocProvider.of<DataTopArtistsCubit>(context).refresh(context);
            },
            icon: const Icon(UniconsSolid.refresh),
          ),
        ],
      ),
      displayBottomNavBar: true,
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageHome();
              },
            );
          },
        ),
        activityPageIndexDiscoveries: ActivityPageItem(
          code: 'page_discoveries',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageDiscoveries();
              },
            );
          },
        ),
        activityPageIndexStatistics: ActivityPageItem(
          code: 'page_statistics',
          icon: Icon(UniconsLine.chart),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageStatistics();
              },
            );
          },
        ),
      },
    ),
  );
}
