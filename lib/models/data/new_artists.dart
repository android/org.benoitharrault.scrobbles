import 'dart:convert';

import 'package:scrobbles/models/data/artists.dart';

class NewArtistData {
  final DateTime? firstPlayed;
  final Artist? artist;

  const NewArtistData({
    required this.firstPlayed,
    required this.artist,
  });

  factory NewArtistData.fromJson(Map<String, dynamic>? json) {
    return NewArtistData(
      firstPlayed: (json?['firstPlayed'] != null && json?['firstPlayed']['date'] != null)
          ? DateTime.parse(
              json?['firstPlayed']['date'],
            )
          : null,
      artist: Artist.fromJson(json?['artist']),
    );
  }
}

class NewArtistsData {
  final List<NewArtistData> data;

  const NewArtistsData({
    required this.data,
  });

  factory NewArtistsData.fromJson(List<dynamic>? json) {
    List<NewArtistData> list = [];

    json?.forEach((item) {
      list.add(NewArtistData.fromJson(item));
    });

    return NewArtistsData(data: list);
  }

  List<Map<String, dynamic>> toJson() {
    List<Map<String, dynamic>> list = [];

    for (var item in data) {
      list.add({
        'firstPlayed': {
          'date': item.firstPlayed?.toString(),
        },
        'artist': item.artist?.toJson(),
      });
    }

    return list;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
