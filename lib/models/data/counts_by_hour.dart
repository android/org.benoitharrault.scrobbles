import 'dart:convert';

class CountsByHourData {
  final Map<int, double> data;

  const CountsByHourData({
    required this.data,
  });

  factory CountsByHourData.fromJson(Map<String, dynamic>? json) {
    Map<int, double> data = {};

    if (json?['counts-by-hour'] != null) {
      json?['counts-by-hour'].keys.forEach((day) {
        if (int.parse(day) != 24) {
          data[int.parse(day)] = double.parse(json['counts-by-hour'][day].toString());
        }
      });
    }

    return CountsByHourData(data: data);
  }

  Map<String, Object?>? toJson() {
    Map<String, double> map = {};

    for (var day in data.keys) {
      double? value = data[day];
      map[day.toString()] = value != null ? value.toDouble() : 0.0;
    }

    return {'counts-by-hour': map};
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
