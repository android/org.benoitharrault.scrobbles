import 'dart:convert';

class Artist {
  final int id;
  final String name;
  final String mbid;

  const Artist({
    required this.id,
    required this.name,
    required this.mbid,
  });

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      id: json['id'] as int,
      name: json['name'] as String,
      mbid: (json['mbid'] != null) ? (json['mbid'] as String) : '',
    );
  }

  Map<String, Object?>? toJson() {
    return {
      'id': id,
      'name': name,
      'mbid': mbid,
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
