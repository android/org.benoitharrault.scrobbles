import 'dart:convert';

import 'package:scrobbles/models/data/artists.dart';

class Track {
  final int id;
  final String name;
  final String mbid;
  final Artist artist;

  const Track({
    required this.id,
    required this.name,
    required this.mbid,
    required this.artist,
  });

  factory Track.fromJson(Map<String, dynamic> json) {
    return Track(
      id: json['id'] as int,
      name: json['name'] as String,
      mbid: (json['mbid'] != null) ? (json['mbid'] as String) : '',
      artist: Artist.fromJson(json['artist']),
    );
  }

  Map<String, Object?>? toJson() {
    return {
      'id': id,
      'name': name,
      'mbid': mbid,
      'artist': artist.toJson(),
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
