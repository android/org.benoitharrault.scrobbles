import 'dart:convert';

class HeatmapData {
  final Map<int, Map<int, int>> data;

  const HeatmapData({
    required this.data,
  });

  factory HeatmapData.fromJson(Map<String, dynamic>? json) {
    Map<int, Map<int, int>> data = {};

    if (json?['heatmap'] != null) {
      json?['heatmap'].keys.forEach((day) {
        Map<String, dynamic> rawDataForThisDay = json['heatmap'][day];

        Map<int, int> dataForThisDay = {};
        for (var hour in rawDataForThisDay.keys) {
          dataForThisDay[int.parse(hour) % 24] = int.parse(rawDataForThisDay[hour].toString());
        }

        data[int.parse(day)] = dataForThisDay;
      });
    }

    return HeatmapData(data: data);
  }

  Map<String, dynamic> toJson() {
    Map<String, Map<String, int>> map = {};

    for (var day in data.keys) {
      Map<String, int> dayMap = {};
      data[day]?.forEach((hour, count) {
        dayMap[(hour % 24).toString()] = count;
      });
      map[day.toString()] = dayMap;
    }

    return {'heatmap': map};
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
