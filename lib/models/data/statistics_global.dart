import 'dart:convert';

class StatisticsGlobalData {
  final int? totalCount;
  final DateTime? lastScrobble;

  const StatisticsGlobalData({
    required this.totalCount,
    required this.lastScrobble,
  });

  factory StatisticsGlobalData.fromJson(Map<String, dynamic>? json) {
    return StatisticsGlobalData(
      totalCount: (json?['totalCount'] != null) ? (json?['totalCount'] as int) : null,
      lastScrobble: (json?['lastScrobble'] != null && json?['lastScrobble']['date'] != null)
          ? DateTime.parse(
              json?['lastScrobble']['date'],
            )
          : null,
    );
  }

  Map<String, Object?>? toJson() {
    return <String, Object?>{
      'totalCount': totalCount,
      'lastScrobble': {
        'date': lastScrobble?.toString(),
      },
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
