import 'dart:convert';

class DiscoveriesDataValue {
  final int newArtistsCount;
  final int newTracksCount;

  const DiscoveriesDataValue({required this.newArtistsCount, required this.newTracksCount});

  factory DiscoveriesDataValue.fromJson(Map<String, dynamic>? json) {
    return DiscoveriesDataValue(
      newArtistsCount: json?['new-artists'] as int,
      newTracksCount: json?['new-tracks'] as int,
    );
  }
}

class DiscoveriesData {
  final Map<String, DiscoveriesDataValue> data;

  const DiscoveriesData({
    required this.data,
  });

  factory DiscoveriesData.fromJson(Map<String, dynamic>? json) {
    Map<String, DiscoveriesDataValue> data = {};

    json?.keys.forEach((date) {
      DiscoveriesDataValue value = DiscoveriesDataValue(
        newArtistsCount: json[date]['new-artists'] as int,
        newTracksCount: json[date]['new-tracks'] as int,
      );

      data[date] = value;
    });

    return DiscoveriesData(data: data);
  }

  Map<String, Object?>? toJson() {
    Map<String, Map<String, int>> map = {};

    for (var element in data.keys) {
      DiscoveriesDataValue? item = data[element];
      map[element] = {
        'new-artists': item != null ? item.newArtistsCount : 0,
        'new-tracks': item != null ? item.newTracksCount : 0,
      };
    }

    return map;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
