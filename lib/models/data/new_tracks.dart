import 'dart:convert';

import 'package:scrobbles/models/data/track.dart';

class NewTrackData {
  final DateTime? firstPlayed;
  final Track? track;

  const NewTrackData({
    required this.firstPlayed,
    required this.track,
  });

  factory NewTrackData.fromJson(Map<String, dynamic>? json) {
    return NewTrackData(
      firstPlayed: (json?['firstPlayed'] != null && json?['firstPlayed']['date'] != null)
          ? DateTime.parse(
              json?['firstPlayed']['date'],
            )
          : null,
      track: Track.fromJson(json?['track']),
    );
  }
}

class NewTracksData {
  final List<NewTrackData> data;

  const NewTracksData({
    required this.data,
  });

  factory NewTracksData.fromJson(List<dynamic>? json) {
    List<NewTrackData> list = [];

    json?.forEach((item) {
      list.add(NewTrackData.fromJson(item));
    });

    return NewTracksData(data: list);
  }

  List<Map<String, dynamic>> toJson() {
    List<Map<String, dynamic>> list = [];

    for (var item in data) {
      list.add({
        'firstPlayed': {
          'date': item.firstPlayed?.toString(),
        },
        'track': item.track?.toJson(),
      });
    }

    return list;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
