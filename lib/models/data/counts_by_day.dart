import 'dart:convert';

class CountsByDayData {
  final Map<int, double> data;

  const CountsByDayData({
    required this.data,
  });

  factory CountsByDayData.fromJson(Map<String, dynamic>? json) {
    Map<int, double> data = {};

    if (json?['counts-by-day'] != null) {
      json?['counts-by-day'].keys.forEach((day) {
        data[int.parse(day)] = double.parse(json['counts-by-day'][day].toString());
      });
    }

    return CountsByDayData(data: data);
  }

  Map<String, Object?>? toJson() {
    Map<String, double> map = {};

    for (var day in data.keys) {
      double? value = data[day];
      map[day.toString()] = value != null ? value.toDouble() : 0.0;
    }

    return {'counts-by-day': map};
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
