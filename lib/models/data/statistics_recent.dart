import 'dart:convert';

class StatisticsRecentData {
  final int? recentCount;
  final int? firstPlayedArtistsCount;
  final int? firstPlayedTracksCount;
  final int? selectedPeriod;

  const StatisticsRecentData({
    required this.recentCount,
    required this.firstPlayedArtistsCount,
    required this.firstPlayedTracksCount,
    required this.selectedPeriod,
  });

  factory StatisticsRecentData.fromJson(Map<String, dynamic>? json) {
    return StatisticsRecentData(
      recentCount: (json?['recentCount'] != null) ? (json?['recentCount'] as int) : null,
      firstPlayedArtistsCount: (json?['firstPlayedArtistsCount'] != null)
          ? (json?['firstPlayedArtistsCount'] as int)
          : null,
      firstPlayedTracksCount: (json?['firstPlayedTracksCount'] != null)
          ? (json?['firstPlayedTracksCount'] as int)
          : null,
      selectedPeriod:
          (json?['selectedPeriod'] != null) ? (json?['selectedPeriod'] as int) : null,
    );
  }

  Map<String, Object?>? toJson() {
    return <String, Object?>{
      'recentCount': recentCount,
      'firstPlayedArtistsCount': firstPlayedArtistsCount,
      'firstPlayedTracksCount': firstPlayedTracksCount,
      'selectedPeriod': selectedPeriod,
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
