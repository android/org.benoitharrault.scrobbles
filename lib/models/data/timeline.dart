import 'dart:convert';

class TimelineDataValue {
  final int counts;
  final int eclecticism;

  const TimelineDataValue({required this.counts, required this.eclecticism});

  factory TimelineDataValue.fromJson(Map<String, dynamic>? json) {
    return TimelineDataValue(
      counts: json?['counts'] as int,
      eclecticism: json?['eclecticism'] as int,
    );
  }
}

class TimelineData {
  final Map<String, TimelineDataValue> data;

  const TimelineData({
    required this.data,
  });

  factory TimelineData.fromJson(Map<String, dynamic>? json) {
    Map<String, TimelineDataValue> data = {};

    json?.keys.forEach((date) {
      TimelineDataValue value = TimelineDataValue(
        counts: json[date]['counts'] as int,
        eclecticism: json[date]['eclecticism'] as int,
      );

      data[date] = value;
    });

    return TimelineData(data: data);
  }

  Map<String, Object?>? toJson() {
    Map<String, Map<String, int>> map = {};

    for (var element in data.keys) {
      TimelineDataValue? item = data[element];
      map[element] = {
        'counts': item != null ? item.counts : 0,
        'eclecticism': item != null ? item.eclecticism : 0,
      };
    }

    return map;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
