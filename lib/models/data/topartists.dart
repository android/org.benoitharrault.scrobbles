import 'dart:convert';

class TopArtistsDataValue {
  final String artistName;
  final int count;

  const TopArtistsDataValue({required this.artistName, required this.count});

  factory TopArtistsDataValue.fromJson(Map<String, dynamic>? json) {
    return TopArtistsDataValue(
      artistName: json?['artistName'] as String,
      count: json?['count'] as int,
    );
  }
}

class TopArtistsStreamDataValue {
  final String artistName;
  final double value;

  const TopArtistsStreamDataValue({
    required this.artistName,
    required this.value,
  });

  factory TopArtistsStreamDataValue.fromJson(Map<String, dynamic>? json) {
    return TopArtistsStreamDataValue(
      artistName: json?['artistName'] as String,
      value: json?['value'] as double,
    );
  }

  Map<String, dynamic>? toJson() {
    return {
      artistName: value,
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

class TopArtistsData {
  final List<TopArtistsDataValue> topArtists;
  final Map<String, List<TopArtistsStreamDataValue>> topArtistsStream;

  const TopArtistsData({
    required this.topArtists,
    required this.topArtistsStream,
  });

  factory TopArtistsData.fromJson(Map<String, dynamic>? json) {
    List<TopArtistsDataValue> topArtists = [];
    Map<String, List<TopArtistsStreamDataValue>> topArtistsStream = {};

    json?['top-artists']?.forEach((element) {
      TopArtistsDataValue value = TopArtistsDataValue(
        artistName: element['artistName'] as String,
        count: element['count'] as int,
      );

      topArtists.add(value);
    });

    json?['top-artists-stream-by-date']?.keys?.forEach((date) {
      if (json['top-artists-stream-by-date'][date] is Map<String, dynamic>) {
        Map<String, dynamic> content = json['top-artists-stream-by-date'][date];

        List<TopArtistsStreamDataValue> items = [];
        content.forEach((String artistName, dynamic rawValue) {
          double value = 0.0;
          if (rawValue is double) {
            value = rawValue;
          } else if (rawValue is int) {
            value = rawValue.toDouble();
          }
          TopArtistsStreamDataValue item = TopArtistsStreamDataValue(
            artistName: artistName,
            value: value,
          );

          items.add(item);
        });

        topArtistsStream[date] = items;
      }
    });

    return TopArtistsData(
      topArtists: topArtists,
      topArtistsStream: topArtistsStream,
    );
  }

  Map<String, Object?>? toJson() {
    List<Map<String, Object>> listArtists = [];
    Map<String, List<Map<String, double>>> artistsStreamMap = {};

    for (TopArtistsDataValue item in topArtists) {
      listArtists.add({
        'artistName': item.artistName,
        'count': item.count,
      });
    }

    for (var dateAsString in topArtistsStream.keys) {
      List<TopArtistsStreamDataValue>? items = topArtistsStream[dateAsString];
      List<Map<String, double>> values = [];
      items?.forEach((item) {
        values.add({
          item.artistName: item.value,
        });
      });
      artistsStreamMap[dateAsString] = values;
    }

    return {
      'top-artists': listArtists,
      'top-artists-stream-by-date': artistsStreamMap,
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
