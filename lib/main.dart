import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:scrobbles/config/application_config.dart';

import 'package:scrobbles/cubit/activity/activity_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_day_cubit.dart';
import 'package:scrobbles/cubit/activity/data_counts_by_hour_cubit.dart';
import 'package:scrobbles/cubit/activity/data_discoveries_cubit.dart';
import 'package:scrobbles/cubit/activity/data_heatmap_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_artists_cubit.dart';
import 'package:scrobbles/cubit/activity/data_new_tracks_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_global_cubit.dart';
import 'package:scrobbles/cubit/activity/data_statistics_recent_cubit.dart';
import 'package:scrobbles/cubit/activity/data_timeline_cubit.dart';
import 'package:scrobbles/cubit/activity/data_top_artists_cubit.dart';

import 'package:scrobbles/ui/skeleton.dart';

void main() async {
  // Initialize packages
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  final Directory tmpDir = await getTemporaryDirectory();
  Hive.init(tmpDir.toString());
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: tmpDir,
  );

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(EasyLocalization(
            path: 'assets/translations',
            supportedLocales: const <Locale>[
              Locale('en'),
              Locale('fr'),
            ],
            fallbackLocale: const Locale('en'),
            useFallbackTranslations: true,
            child: const MyApp(),
          )));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        // default providers
        BlocProvider<NavCubitPage>(
          create: (context) => NavCubitPage(appConfig: ApplicationConfig.config),
        ),
        BlocProvider<NavCubitScreen>(
          create: (context) => NavCubitScreen(),
        ),
        BlocProvider<ApplicationThemeModeCubit>(
          create: (context) => ApplicationThemeModeCubit(),
        ),
        BlocProvider<ActivityCubit>(
          create: (context) => ActivityCubit(),
        ),
        BlocProvider<ActivitySettingsCubit>(
          create: (context) => ActivitySettingsCubit(appConfig: ApplicationConfig.config),
        ),

        // custom providers
        BlocProvider<DataCountsByDayCubit>(
          create: (context) => DataCountsByDayCubit(),
        ),
        BlocProvider<DataCountsByHourCubit>(
          create: (context) => DataCountsByHourCubit(),
        ),
        BlocProvider<DataDiscoveriesCubit>(
          create: (context) => DataDiscoveriesCubit(),
        ),
        BlocProvider<DataHeatmapCubit>(
          create: (context) => DataHeatmapCubit(),
        ),
        BlocProvider<DataNewArtistsCubit>(
          create: (context) => DataNewArtistsCubit(),
        ),
        BlocProvider<DataNewTracksCubit>(
          create: (context) => DataNewTracksCubit(),
        ),
        BlocProvider<DataStatisticsGlobalCubit>(
          create: (context) => DataStatisticsGlobalCubit(),
        ),
        BlocProvider<DataStatisticsRecentCubit>(
          create: (context) => DataStatisticsRecentCubit(),
        ),
        BlocProvider<DataTimelineCubit>(
          create: (context) => DataTimelineCubit(),
        ),
        BlocProvider<DataTopArtistsCubit>(
          create: (context) => DataTopArtistsCubit(),
        ),
      ],
      child: BlocBuilder<ApplicationThemeModeCubit, ApplicationThemeModeState>(
        builder: (BuildContext context, ApplicationThemeModeState state) {
          return MaterialApp(
            title: ApplicationConfig.config.appTitle,
            home: const SkeletonScreen(),

            // Theme stuff
            theme: lightTheme,
            darkTheme: darkTheme,
            themeMode: state.themeMode,

            // Localization stuff
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            debugShowCheckedModeBanner: false,
          );
        },
      ),
    );
  }
}
