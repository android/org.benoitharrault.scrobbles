import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:scrobbles/models/data/counts_by_day.dart';
import 'package:scrobbles/models/data/counts_by_hour.dart';
import 'package:scrobbles/models/data/discoveries.dart';
import 'package:scrobbles/models/data/heatmap.dart';
import 'package:scrobbles/models/data/new_artists.dart';
import 'package:scrobbles/models/data/new_tracks.dart';
import 'package:scrobbles/models/data/statistics_global.dart';
import 'package:scrobbles/models/data/statistics_recent.dart';
import 'package:scrobbles/models/data/timeline.dart';
import 'package:scrobbles/models/data/topartists.dart';

class ScrobblesApi {
  static String baseUrl = 'https://scrobble.harrault.fr';

  static Future<StatisticsGlobalData?> fetchGlobalStatistics() async {
    final String url = '$baseUrl/stats';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return StatisticsGlobalData.fromJson(rawData);
    }

    return null;
  }

  static Future<StatisticsRecentData?> fetchRecentStatistics(int daysCount) async {
    final String url = '$baseUrl/$daysCount/stats';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return StatisticsRecentData.fromJson(rawData);
    }

    return null;
  }

  static Future<TimelineData?> fetchTimeline(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/timeline';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return TimelineData.fromJson(rawData);
    }

    return null;
  }

  static Future<CountsByDayData?> fetchCountsByDay(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/counts-by-day';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return CountsByDayData.fromJson(rawData);
    }

    return null;
  }

  static Future<CountsByHourData?> fetchCountsByHour(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/counts-by-hour';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return CountsByHourData.fromJson(rawData);
    }

    return null;
  }

  static Future<DiscoveriesData?> fetchDiscoveries(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/news';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return DiscoveriesData.fromJson(rawData);
    }

    return null;
  }

  static Future<TopArtistsData?> fetchTopArtists(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/top-artists';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return TopArtistsData.fromJson(rawData);
    }

    return null;
  }

  static Future<HeatmapData?> fetchHeatmap(int daysCount) async {
    final String url = '$baseUrl/data/$daysCount/heatmap';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> rawData = jsonDecode(response.body) as Map<String, dynamic>;
      return HeatmapData.fromJson(rawData);
    }

    return null;
  }

  static Future<NewArtistsData?> fetchNewArtists(int count) async {
    final String url = '$baseUrl/data/discoveries/artists/$count';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final List<dynamic> rawData = jsonDecode(response.body) as List<dynamic>;
      return NewArtistsData.fromJson(rawData);
    }

    return null;
  }

  static Future<NewTracksData?> fetchNewTracks(int count) async {
    final String url = '$baseUrl/data/discoveries/tracks/$count';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final List<dynamic> rawData = jsonDecode(response.body) as List<dynamic>;
      return NewTracksData.fromJson(rawData);
    }

    return null;
  }
}
